# Installation guide UNIX Environment

**Install Maven on your local machine**

1. Download apache maven from [Maven download](https://www-us.apache.org/dist/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip)
2. Unzip file apache-maven-3.6.1-bin.zip
3. Add apache-maven-3.6.1/bin to environment variable so you can just type "mvn" on your terminal 
> export PATH=$PATH:<apache maven bin folder>
> example : export PATH=$PATH:/Applications/apache-maven-3.6.1/bin
4. Run command "cd" to root application
> cd exampletask
5. Run command 
> bin/setup to compile and run unit testing.
6. Run command 
> bin/start to start application.
