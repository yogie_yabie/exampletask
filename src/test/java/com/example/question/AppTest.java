package com.example.question;

import com.example.question.entities.Cart;
import com.example.question.util.Question1Task;
import com.example.question.util.Question3Task;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;


public class AppTest 
{
    //Testing Question 1
    @Test
    public void testLoadValueTrue() {
        HashMap[] a;
        Question1Task tester = new Question1Task();
        String text = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB";
        a = tester.load(text);

        assertEquals("value2", a[0].get("key2"));
        assertEquals("valueB", a[2].get("keyB"));
        assertEquals("valueA", a[1].get("keyA"));
    }

    //Testing Question 1
    @Test
    public void testLoadValueFalse() {
        HashMap[] a;
        Question1Task tester = new Question1Task();
        String text = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB";
        a = tester.load(text);

        assertFalse(a[0].get("key2").equals("value9"));
    }

    //Testing Question 1
    @Test
    public void testStoreValueTrue() {
        HashMap[] a;
        Question1Task tester = new Question1Task();
        String text = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB";
        a = tester.load(text);

        text = tester.store(a);

        assertEquals("key1=value1;key2=value2\\nkeyA=valueA\\nkeyB=valueB", text);

    }

    //Testing Question 1
    @Test
    public void testStoreValueFalse() {
        HashMap[] a;
        Question1Task tester = new Question1Task();
        String text = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB";
        a = tester.load(text);
        text = tester.store(a);
        assertFalse(text.equals("key1=value1;key2=value2\nkeyA=valueB\nkeyB=valueA"));
    }


    //Testing Question 3
    //Customer employee get 30%
    @Test
    public void testEmployeeCustomerTrue(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID4", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID4", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID4", "3", "ID3");
        list_cart.add(cart);
        assertEquals("240.0",String.valueOf(task2.buy(list_cart)));
    }

    //Testing Question 3
    @Test
    public void testEmployeeCustomerFalse(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID4", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID4", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID4", "3", "ID3");
        list_cart.add(cart);
        assertFalse(String.valueOf(task2.buy(list_cart)).equals("300.0"));
    }

    //Testing Question 3
    //Customer affiliate get 10%
    @Test
    public void testAffiliateCustomerTrue(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID7", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID7", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID7", "3", "ID3");
        list_cart.add(cart);
        assertEquals("280.0",String.valueOf(task2.buy(list_cart)));
    }

    //Testing Question 3
    @Test
    public void testAffiliateCustomerFalse(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID7", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID7", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID7", "3", "ID3");
        list_cart.add(cart);
        assertFalse(String.valueOf(task2.buy(list_cart)).equals("300.0"));
    }

    //Testing Question 3
    //Customer user has been a customer for over 2 years, he gets a 5% discount.
    @Test
    public void testMemberCustomerTrue(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID1", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID1", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID1", "3", "ID3");
        list_cart.add(cart);
        assertEquals("290.0",String.valueOf(task2.buy(list_cart)));
    }

    //Testing Question 3
    @Test
    public void testMemberCustomerFalse(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID1", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID1", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID1", "3", "ID3");
        list_cart.add(cart);
        assertFalse(String.valueOf(task2.buy(list_cart)).equals("300.0"));
    }

    //Testing Question 3
    //For every $100 on the bill, there would be a $ 5 discount .
    @Test
    public void testBillCustomerTrue(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID2", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID2", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID2", "3", "ID3");
        list_cart.add(cart);
        assertEquals("285.0",String.valueOf(task2.buy(list_cart)));
    }

    //Testing Question 3
    @Test
    public void testBillCustomerFalse(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID2", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID2", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID2", "3", "ID3");
        list_cart.add(cart);
        assertFalse(String.valueOf(task2.buy(list_cart)).equals("300.0"));
    }

    //Testing Question 3
    //Less than $100 on the bill, there is no discount.
    @Test
    public void testCustomerTrue(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID2", "1", "ID7");
        list_cart.add(cart);
        assertEquals("50.0",String.valueOf(task2.buy(list_cart)));
    }

    //Testing Question 3
    @Test
    public void testCustomerFalse(){
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID2", "1", "ID7");
        list_cart.add(cart);
        assertFalse(String.valueOf(task2.buy(list_cart)).equals("45.0"));
    }


}
