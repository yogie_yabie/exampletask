package com.example.question.util;

import com.example.question.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Question3Task implements Retail {
    private final Logger mLogger = LoggerFactory.getLogger(Question3Task.class);

    @Override
    public double buy(List<Cart> carts) {
        HashMap<String, Product> get_product = loadProduct();
        HashMap<String, Customer> get_customer = loadCustomer();
        HashMap<String, Employee> get_employee = loadEmployee();
        HashMap<String, Affiliate> get_affiliate = loadAffiliate();
        mLogger.info("List purchase product:");
        double net_payable_amount = 0;
        String message = "";
        for (Cart item : carts) {
            Product get_prod = get_product.get(item.getProdId());

            if (get_employee.get(item.getCustId()) != null) {
                Employee get_item = get_employee.get(item.getCustId());
                if (!isGrocery(get_prod)) {
                    int discount = calculateDiscountEmployee(get_prod.getProdPrice());
                    message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice()
                            + " Discount Employee 30% = $" + discount;
                    double amount_after_discount = get_prod.getProdPrice() - discount;
                    net_payable_amount += amount_after_discount;
                } else {
                    message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice();
                    net_payable_amount += get_prod.getProdPrice();
                }
            } else if (get_affiliate.get(item.getCustId()) != null) {
                Affiliate get_item = get_affiliate.get(item.getCustId());
                if (!isGrocery(get_prod)) {
                    int discount = calculateDiscountAffiliate(get_prod.getProdPrice());
                    message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice()
                            + " Discount Affiliate 10% = $" + discount;
                    double amount_after_discount = get_prod.getProdPrice() - discount;
                    net_payable_amount += amount_after_discount;
                } else {
                    message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice();
                    net_payable_amount += get_prod.getProdPrice();
                }

            } else if (get_customer.get(item.getCustId()) != null) {
                Customer get_item = get_customer.get(item.getCustId());
                if (calculateMember(get_item.getJoinDate()) == 2) {
                    if (!isGrocery(get_prod)) {
                        int discount = calculateDiscount2Years(get_prod.getProdPrice());
                        message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice()
                                + " Discount 2 Years Customer 5% = $" + discount;
                        double amount_after_discount = get_prod.getProdPrice() - discount;
                        net_payable_amount += amount_after_discount;
                    } else {
                        message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice();
                        net_payable_amount += get_prod.getProdPrice();
                    }
                } else {
                    if (get_prod.getProdPrice() >= 100) {
                        int discount = calculateDiscountBill(get_prod.getProdPrice());
                        message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice()
                                + " Discount 2 Years Customer $5 every $100 = $" + discount;
                        double amount_after_discount = get_prod.getProdPrice() - discount;
                        net_payable_amount += amount_after_discount;
                    } else {
                        message += "Product Name: " + get_prod.getProdName() + " Price: $" + get_prod.getProdPrice();
                        net_payable_amount += get_prod.getProdPrice();
                    }

                }

            } else {
                message += "Customer not found register first";
            }
            message += "\n";

        }
        mLogger.info(message);
        mLogger.info("Total Net Payable Amount = $" + net_payable_amount);
        return net_payable_amount;

    }

    public boolean isGrocery(Product prod) {
        if (!prod.getProdType().equalsIgnoreCase("grocery")) {
            return false;
        } else return true;
    }

    @Override
    public HashMap<String, Product> loadProduct() {
        mLogger.info("loadProduct()");
        HashMap<String, Product> get_product = new HashMap<>();

        Product product = new Product("ID1", "Silverqueen Chunky Bar", "Food", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID2", "Ultra Milk 1L", "Food", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID3", "Eggs", "Grocery", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID4", "Mustard", "Grocery", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID5", "Barbecue sauce", "Grocery", 50);
        get_product.put(product.getProdId(), product);
        product = new Product("ID6", "Tomato sauce", "Grocery", 50);
        get_product.put(product.getProdId(), product);
        product = new Product("ID7", "Indomie Rebus", "Food", 50);
        get_product.put(product.getProdId(), product);
        product = new Product("ID8", "Indomie Goreng", "Food", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID9", "Rice 1Kg", "Food", 100);
        get_product.put(product.getProdId(), product);
        product = new Product("ID10", "Mango 1Kg", "Food", 100);
        get_product.put(product.getProdId(), product);
        return get_product;
    }

    @Override
    public HashMap<String, Customer> loadCustomer() {
        mLogger.info("loadCustomer()");
        Calendar cal = Calendar.getInstance();
        cal.set(2017, 01, 01);
        HashMap<String, Customer> get_customer = new HashMap<>();

        Customer list_customer = new Customer("CID1", "Agus Laksono", "0812345678910", "Jakarta", cal.getTime());
        get_customer.put(list_customer.getCustId(), list_customer);
        cal.set(2019, 01, 01);
        list_customer = new Customer("CID2", "Made Dharmawan", "0812345678910", "Jakarta", cal.getTime());
        get_customer.put(list_customer.getCustId(), list_customer);
        list_customer = new Customer("CID3", "Anton S", "0812345678910", "Jakarta", cal.getTime());
        get_customer.put(list_customer.getCustId(), list_customer);
        return get_customer;
    }

    @Override
    public HashMap<String, Affiliate> loadAffiliate() {
        mLogger.info("loadAffiliate()");
        Calendar cal = Calendar.getInstance();
        cal.set(2019, 01, 01);
        HashMap<String, Affiliate> get_affiliate = new HashMap<>();

        Affiliate list_affiliate = new Affiliate("CID7", "Fika", "0811", "Jakarta", cal.getTime(), "2001", "AlfaMart_1", "Supplier");
        get_affiliate.put(list_affiliate.getCustId(), list_affiliate);
        list_affiliate = new Affiliate("CID8", "Fadil", "0811", "Jakarta", cal.getTime(), "2001", "AlfaMart_2", "Supplier");
        get_affiliate.put(list_affiliate.getCustId(), list_affiliate);
        return get_affiliate;
    }

    @Override
    public HashMap<String, Employee> loadEmployee() {
        mLogger.info("loadEmployee");
        Calendar cal = Calendar.getInstance();
        cal.set(2019, 01, 01);
        HashMap<String, Employee> get_employee = new HashMap<>();
        Employee list_employe = new Employee("CID4", "Indra", "0811", "Jakarta", cal.getTime(), "1001", "Staff");
        get_employee.put(list_employe.getCustId(), list_employe);
        list_employe = new Employee("CID5", "Venta", "0811", "Jakarta", cal.getTime(), "1002", "Staff");
        get_employee.put(list_employe.getCustId(), list_employe);
        list_employe = new Employee("CID6", "Anwar", "0811", "Jakarta", cal.getTime(), "1003", "Staff");
        get_employee.put(list_employe.getCustId(), list_employe);
        return get_employee;
    }

    //Calculate member age
    @Override
    public int calculateMember(Date joinDate) {

        Calendar cal_joinDate = Calendar.getInstance();
        cal_joinDate.setTime(joinDate);
        int year = cal_joinDate.get(Calendar.YEAR);
        int month = cal_joinDate.get(Calendar.MONTH);
        int day = cal_joinDate.get(Calendar.DAY_OF_MONTH);
        LocalDate join_date = LocalDate.of(year, month, day);
        LocalDate current_date = LocalDate.now();
        Period period = Period.between(join_date, current_date);

        return period.getYears();
    }

    //Calculate discount member 2 years (5%)
    @Override
    public int calculateDiscount2Years(double price) {
        int discount = (int) Math.floor((price * 5) / 100);
        return discount;
    }

    //Calculate discount employee (30%)
    @Override
    public int calculateDiscountEmployee(double price) {
        int discount = (int) Math.floor((price * 30) / 100);
        return discount;
    }

    //Calculate discount affiliate (10%)
    @Override
    public int calculateDiscountAffiliate(double price) {
        int discount = (int) Math.floor((price * 10) / 100);
        return discount;
    }

    //Calculate discount bill every $100 get $5 discount
    @Override
    public int calculateDiscountBill(double price) {
        int discount = (int) Math.floor(price / 100);
        int val = discount * 5;
        return val;
    }


}
