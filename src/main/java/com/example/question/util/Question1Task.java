package com.example.question.util;

import com.example.question.entities.StoreLoad;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Question1Task implements StoreLoad {

    final Logger mLogger = LoggerFactory.getLogger(Question1Task.class);
    private HashMap<String, String>[] map = new HashMap[10];


    @Override
    public String store(HashMap<String, String>[] a) {
        String concat = "";
        int i = 0;
        for (HashMap<String, String> text_encode : a) {
            if (i != 0 && text_encode != null) {
                concat += "\\n";
            }
            try {
                int j = 1;
                for (Map.Entry<String, String> entry : text_encode.entrySet()) {
                    if (text_encode.size() != 1) {
                        if (j != text_encode.size()) {
                            concat += entry.getKey() + "=" + entry.getValue() + ";";
                        } else {
                            concat += entry.getKey() + "=" + entry.getValue();
                        }

                    } else {
                        concat += entry.getKey() + "=" + entry.getValue();
                    }
                    j++;

                }
            } catch (NullPointerException ex) {

            }

            i++;
        }
        return concat;
    }

    @Override
    public HashMap<String, String>[] load(String text) {
        try {

            String[] explode_text = text.split("\\n");
            int i = 0;
            for (String item : explode_text) {

                String[] item_detail = item.split(";");
                getMap()[i] = new HashMap();
                for (String item_key : item_detail) {
                    String[] item_key_value = item_key.split("=");
                    getMap()[i].put(item_key_value[0], item_key_value[1]);
                    mLogger.info("a[" + i + "] Key : " + item_key_value[0] + " Value: " + item_key_value[1] + " Stored");
                }
                i++;
            }
            return getMap();
        } catch (Exception ex) {
            mLogger.error("Invalid input ", ex);
        }
        return null;

    }

    public HashMap<String, String>[] getMap() {
        return map;
    }
}
