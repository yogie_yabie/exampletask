package com.example.question.entities;

public class Cart {
    private String custId;
    private String cartId;
    private String prodId;


    public Cart(String custId, String cartId, String prodId) {
        this.custId = custId;
        this.cartId = cartId;
        this.prodId = prodId;

    }

    public String getCustId() {
        return custId;
    }

    public String getCartId() {
        return cartId;
    }

    public String getProdId() {
        return prodId;
    }

}
