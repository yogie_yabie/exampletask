package com.example.question.entities;

public class Product {
    private String prodId;
    private String prodName;
    private String prodType;
    private double prodPrice;

    public Product() {
    }

    public Product(String prodId, String prodName, String prodType, double prodPrice) {
        this.prodId = prodId;
        this.prodName = prodName;
        this.prodType = prodType;
        this.prodPrice = prodPrice;
    }

    public String getProdId() {
        return prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public String getProdType() {
        return prodType;
    }

    public double getProdPrice() {
        return prodPrice;
    }
}
