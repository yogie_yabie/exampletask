package com.example.question.entities;

import java.util.Date;

public class Employee extends Customer{

    private String nik;
    private String officePosition;

    public Employee(String custId, String name, String phoneNumber, String address, Date joinDate, String nik, String officePosition) {
        super(custId, name, phoneNumber, address, joinDate);
        this.nik = nik;
        this.officePosition = officePosition;
    }

    public String getNik() {
        return nik;
    }

    public String getOfficePosition() {
        return officePosition;
    }
}
