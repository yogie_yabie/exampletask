package com.example.question.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface Retail {
    HashMap<String,Product> loadProduct();
    HashMap<String,Customer> loadCustomer();
    HashMap<String,Affiliate> loadAffiliate();
    HashMap<String,Employee> loadEmployee();
    int calculateMember(Date joinDate);
    int calculateDiscount2Years(double price);
    int calculateDiscountEmployee(double price);
    int calculateDiscountAffiliate(double price);
    int calculateDiscountBill(double price);
    double buy(List<Cart> carts);
}
