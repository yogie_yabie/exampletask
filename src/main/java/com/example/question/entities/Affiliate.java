package com.example.question.entities;

import java.util.Date;

public class Affiliate extends Customer{

    private String affId;
    private String affName;
    private String occupation;

    public Affiliate(String custId, String name, String phoneNumber, String address, Date joinDate, String affId, String affName, String occupation) {
        super(custId, name, phoneNumber, address, joinDate);
        this.affId = affId;
        this.affName = affName;
        this.occupation = occupation;
    }

    public String getAffId() {
        return affId;
    }

    public String getAffName() {
        return affName;
    }

    public String getOccupation() {
        return occupation;
    }
}
