package com.example.question.entities;

import java.util.Date;

public class Customer {
    private String custId;
    private String name;
    private String phoneNumber;
    private String address;
    private Date joinDate;


    public Customer() {
    }

    public Customer(String custId, String name, String phoneNumber, String address, Date joinDate) {
        this.custId = custId;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.joinDate = joinDate;
    }

    public String getCustId() {
        return custId;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public Date getJoinDate() {
        return joinDate;
    }
}
