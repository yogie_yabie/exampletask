package com.example.question.entities;

import java.util.HashMap;

public interface StoreLoad {
    String store(HashMap<String, String>[] a);
    HashMap<String,String>[] load(String key);
}
