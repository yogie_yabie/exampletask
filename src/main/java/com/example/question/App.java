package com.example.question;

import com.example.question.entities.*;
import com.example.question.util.Question1Task;
import com.example.question.util.Question3Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class App {

    final static Logger mLogger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        // Question 1
        mLogger.info("======== Question 1 ========");
        HashMap[] a;
        String text = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB";
        mLogger.info("Input text = " + text);

        Question1Task task1 = new Question1Task();
        mLogger.info("a=load(text);");
        a = task1.load(text);
        text = task1.store(a);
        mLogger.info("text=store(a)");
        mLogger.info("Result text = " + text);

        // Question 3
        mLogger.info("======== Question 3 ========");
        Question3Task task2 = new Question3Task();
        List<Cart> list_cart = new ArrayList<>();
        Cart cart = new Cart("CID4", "1", "ID1");
        list_cart.add(cart);
        cart = new Cart("CID4", "2", "ID2");
        list_cart.add(cart);
        cart = new Cart("CID4", "3", "ID3");
        list_cart.add(cart);
        task2.buy(list_cart);

    }

}
